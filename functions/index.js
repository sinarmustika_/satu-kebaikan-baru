const functions = require('firebase-functions');
const admin = require('firebase-admin');
const app = require('firebase-admin');
admin.initializeApp({
  credential: admin.credential.applicationDefault()
});

const db = admin.firestore();

exports.ButuhDanaSegera = functions.https.onRequest(async (req, res) => {
  const snapshot = await admin.firestore().collection("Config").limit(6).get();
  let Config = [];
  snapshot.forEach(butuhDanaSegera => {
    let id = butuhDanaSegera.id;
    let data = butuhDanaSegera.data();
    let sisaHari = data.tanggalSelesai - data.tanggalMulai;
    Config.push({id, ...data, sisaHari});
  });
  res.status(200).send(JSON.stringify(Config));
})

exports.DonasiPilihan = functions.https.onRequest(async (req, res) => {
  const snapshot = await admin.firestore().collection("Config").limit(6).get();
  let Config = [];
  snapshot.forEach(DonasiPilihan => {
    let id = DonasiPilihan.id;
    let data = DonasiPilihan.data();
    let sisaHari = data.tanggalSelesai - data.tanggalMulai;
    Config.push({id, ...data, sisaHari});
  });
  res.status(200).send(JSON.stringify(Config));
})

exports.BantuSiapaHariIni = functions.https.onRequest(async (req, res) => {
  const snapshot = await admin.firestore().collection("Config").limit(6).get();
  let Config = [];
  snapshot.forEach(BantuSiapaHariIni => {
    let id = BantuSiapaHariIni.id;
    let data = BantuSiapaHariIni.data();
    let sisaHari = data.tanggalSelesai - data.tanggalMulai;
    Config.push({id, ...data, sisaHari});
  });
  res.status(200).send(JSON.stringify(Config));
})

exports.Campaigns = functions.https.onRequest(async (req, res) => {
  try {
    const snapshot = await admin.firestore().collection("Campaign").limit(6).get();
  // const snapshot2 = await admin.firestore().collection("Campaign/docId/Donasi").limit(6).get();
  // const snapshot3 = await snapshot1 + snapshot2;
  let Campaign = [];
  snapshot.forEach(Campaigns => {
    let id = Campaigns.id;
    let data = Campaigns.data();
    Campaign.push({id, ...data});
  });
  res.status(200).send(JSON.stringify(Campaign));
  } catch (error) {
    res.status(404).json({message: "Campaign tidak ditemukan"})
  }
 })

// Ini hanya untuk uji coba
exports.Users = functions.https.onRequest(async (req, res) => {
 const snapshot = await admin.firestore().collection("User").limit(6).get();
 let User = [];
 snapshot.forEach(Users => {
   let id = Users.id;
   let data = Users.data();
   User.push({id, ...data});
 });
 res.status(200).send(JSON.stringify(User));
})

